/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

$(document).ready(function () {
    let toasts = $(".toast");
    setTimeout(function () {
        toasts.show();
    }, 700);

    setTimeout(function () {
        toasts.hide();
    }, 5000);

    $('.delete').on('click', function (e) {
        e.preventDefault();

        let name = $(this).data('name'),
            href = $(this).attr('href');

        if (confirm('Вы действительно хотите удалить ' + name + '?') === false) {
            return false;
        }
        window.location = href;
    });
});
