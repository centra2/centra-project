<?php

namespace App\Entity;

use App\Repository\GoodRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @ORM\Entity(repositoryClass=GoodRepository::class)
 */
class Good
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="goods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function insert(Request $request, ValidatorInterface $validator, ManagerRegistry $doctrine): array
    {
        $post = $request->request;
        $category = $doctrine->getRepository(Category::class)->find($post->get('category'));

        if ($category == null) {
            return [
                'status' => 'error',
                'message' => "Категория с идентификатором " . $post->get('category') . " не найдена"
            ];
        }

        $this->setName($post->get('name'));
        $this->setPrice($post->get('price'));
        $this->setCategory($category);

        $errors = $validator->validate($this);
        if (count($errors) > 0) {
            return [
                'status' => 'error',
                'message' => (string)$errors
            ];
        }

        $good_repository = new GoodRepository($doctrine);
        $good_repository->add($this, true);

        return [
            'status' => 'success'
        ];
    }
}
