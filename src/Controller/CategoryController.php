<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Good;
use App\Repository\CategoryRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends AbstractController
{
    public const ITEMS_PER_ADMIN_PAGE = 10;

    /**
     * @Route("/category/{id<\d+>?1}", name="category")
     */
    public function index(ManagerRegistry $doctrine, int $id): Response
    {
        $category_repository = $doctrine->getRepository(Category::class);

        $categories = $category_repository->findAll();
        $active_category = $category_repository->find($id);

        if ($active_category == null) {
            $this->addFlash(
                "notice",
                "Категория с идентификатором " . $id . " не найдена"
            );

            return $this->redirectToRoute('categories');
        }

        return $this->render('category/index.html.twig', [
            'active_category' => $active_category,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/admin/categories/{page<\d+>?1}", name="admin_categories")
     */
    public function admin_index(ManagerRegistry $doctrine, int $page): Response
    {
        $categories_count = $doctrine->getRepository(Good::class)->countAll();
        $pages_count = ceil($categories_count / $this::ITEMS_PER_ADMIN_PAGE);

        if ($page > $pages_count && $categories_count != 0) {
            return $this->redirectToRoute('admin_categories', ['page' => $pages_count]);
        }

        $categories = $doctrine->getRepository(Category::class)->getPage($page, $this::ITEMS_PER_ADMIN_PAGE);

        return $this->render('category/admin/index.html.twig', [
            'categories' => $categories,
            'pages_count' => $pages_count,
            'current_page' => $page
        ]);
    }

    /**
     * @Route("/admin/categories/add", name="admin_category_add")
     */
    public function admin_add(ManagerRegistry $doctrine): Response
    {
        return $this->render('category/admin/add.html.twig');
    }

    /**
     * @Route("/categories/add", name="category_add")
     */
    public function add(Request $request, ValidatorInterface $validator, ManagerRegistry $doctrine): Response
    {
        $category = new Category();
        $response = $category->insert($request, $validator, $doctrine);

        if ($response['status'] == 'error') {
            return new Response($response['message'], 400);
        }

        $this->addFlash(
            "notice",
            "Категория успешно добавлена"
        );

        return $this->redirectToRoute('admin_categories');
    }

    /**
     * @Route("/admin/categories/edit/{id<\d+>?1}", name="admin_category_edit")
     */
    public function admin_edit(ManagerRegistry $doctrine, int $id): Response
    {
        $category = $doctrine->getRepository(Category::class)->find($id);

        if ($category == null) {
            $this->addFlash(
                "notice",
                "Категория с идентификатором " . $id . " не найдена"
            );

            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('category/admin/edit.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/categories/edit", name="category_edit")
     */
    public function edit(Request $request, ValidatorInterface $validator, ManagerRegistry $doctrine): Response
    {
        $data = $request->request;
        $id = $data->get('id');

        $category = $doctrine->getManager()->getRepository(Category::class)->find($id);

        if ($category == null) {
            $this->addFlash(
                "notice",
                "Категория с идентификатором " . $data->get('id') . " не найдена"
            );

            return $this->redirectToRoute('admin_categories');
        }

        $response = $category->insert($request, $validator, $doctrine);

        if ($response['status'] == 'error') {
            return new Response($response['message'], 400);
        }

        $this->addFlash(
            "notice",
            "Категория успешно отредактирована"
        );

        return $this->redirectToRoute('admin_categories');
    }

    /**
     * @Route("/categories/delete/{id<\d+>}", name="category_delete")
     */
    public function delete(int $id, ManagerRegistry $doctrine): Response
    {
        $category = $doctrine->getManager()->getRepository(Category::class)->find($id);

        if ($category == null) {
            $this->addFlash(
                "notice",
                "Категория с идентификатором " . $id . " не найдена"
            );

            return $this->redirectToRoute('admin_categories');
        }

        if (count($category->getGoods()) > 0) {
            $this->addFlash(
                "notice",
                "Категория содержит товары. Удалите товары, после чего удалите категорию"
            );

            return $this->redirectToRoute('admin_categories');
        }

        $category_repository = new CategoryRepository($doctrine);
        $category_repository->remove($category, true);

        $this->addFlash(
            "notice",
            "Категория успешно удалена"
        );

        return $this->redirectToRoute('admin_categories');
    }
}
