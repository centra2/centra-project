<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user/register", name="register")
     */
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher, ValidatorInterface $validator, ManagerRegistry $doctrine)
    {
        if (count($request->request) == 0) {
            return $this->render('user/register.html.twig');
        }

        $user = new User();
        $response = $user->insert($request, $validator, $doctrine, $passwordHasher);

        if ($response['status'] == 'error') {
            $this->addFlash(
                "notice",
                $response['message']
            );

            return $this->render('user/register.html.twig');
        }

        $this->addFlash(
            "notice",
            "Вы успешно зарегистрированы"
        );

        return $this->render('user/login.html.twig');
    }

    /**
     * @Route("/user/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/user/logout", name="logout", methods={"GET"})
     */
    public function logout(): void
    {
    }
}
