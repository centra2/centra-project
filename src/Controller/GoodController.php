<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Good;
use App\Repository\GoodRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GoodController extends AbstractController
{
    public const ITEMS_PER_PAGE = 8;
    public const ITEMS_PER_ADMIN_PAGE = 10;

    /**
     * @Route("/{page<\d+>?1}", name="goods")
     */
    public function index(ManagerRegistry $doctrine, int $page): Response
    {
        $goods_count = $doctrine->getRepository(Good::class)->countAll();
        $pages_count = ceil($goods_count / $this::ITEMS_PER_PAGE);

        if ($page > $pages_count && $goods_count != 0) {
            return $this->redirectToRoute('goods', ['page' => $pages_count]);
        }

        $goods = $doctrine->getRepository(Good::class)->getPage($page, $this::ITEMS_PER_PAGE);

        return $this->render('good/index.html.twig', [
            'goods' => $goods,
            'pages_count' => $pages_count,
            'current_page' => $page
        ]);
    }

    /**
     * @Route("/admin/goods/{page<\d+>?1}", name="admin_goods")
     */
    public function admin_index(ManagerRegistry $doctrine, int $page): Response
    {
        $goods_count = $doctrine->getRepository(Good::class)->countAll();
        $pages_count = ceil($goods_count / $this::ITEMS_PER_ADMIN_PAGE);

        if ($page > $pages_count && $goods_count != 0) {
            return $this->redirectToRoute('admin_goods', ['page' => $pages_count]);
        }

        $goods = $doctrine->getRepository(Good::class)->getPage($page, $this::ITEMS_PER_ADMIN_PAGE);

        return $this->render('good/admin/index.html.twig', [
            'goods' => $goods,
            'pages_count' => $pages_count,
            'current_page' => $page
        ]);
    }

    /**
     * @Route("/admin/goods/add", name="admin_good_add")
     */
    public function admin_add(ManagerRegistry $doctrine): Response
    {
        $categories = $doctrine->getRepository(Category::class)->findAll();

        if (count($categories) == 0) {
            $this->addFlash(
                "notice",
                "Категории товаров не найдены. Создание товара невозможно"
            );

            return $this->redirectToRoute('admin_goods');
        }

        return $this->render('good/admin/add.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/goods/add", name="good_add")
     */
    public function add(Request $request, ValidatorInterface $validator, ManagerRegistry $doctrine): Response
    {
        $good = new Good();
        $response = $good->insert($request, $validator, $doctrine);

        if ($response['status'] == 'error') {
            return new Response($response['message'], 400);
        }

        $this->addFlash(
            "notice",
            "Товар успешно добавлен"
        );

        return $this->redirectToRoute('admin_goods');
    }

    /**
     * @Route("/admin/goods/edit/{id<\d+>?1}", name="admin_good_edit")
     */
    public function admin_edit(ManagerRegistry $doctrine, int $id): Response
    {
        $good = $doctrine->getRepository(Good::class)->find($id);

        if ($good == null) {
            $this->addFlash(
                "notice",
                'Товар с идентификатором ' . $id . ' не найден'
            );

            return $this->redirectToRoute('admin_goods');
        }

        $categories = $doctrine->getRepository(Category::class)->findAll();

        if (count($categories) == 0) {
            $this->addFlash(
                "notice",
                "Категории товаров не найдены. Редактирование товара невозможно"
            );

            return $this->redirectToRoute('admin_goods');
        }

        return $this->render('good/admin/edit.html.twig', [
            'good' => $good,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/goods/edit", name="good_edit")
     */
    public function edit(Request $request, ValidatorInterface $validator, ManagerRegistry $doctrine): Response
    {
        $data = $request->request;
        $good = $doctrine->getRepository(Good::class)->find($data->get('id'));

        if ($good == null) {
            $this->addFlash(
                "notice",
                "Товар с идентификатором " . $data->get('id') . " не найден"
            );

            return $this->redirectToRoute('admin_goods');
        }

        $response = $good->insert($request, $validator, $doctrine);

        if ($response['status'] == 'error') {
            return new Response($response['message'], 400);
        }

        $this->addFlash(
            "notice",
            "Товар успешно отредактирован"
        );

        return $this->redirectToRoute('admin_goods');
    }

    /**
     * @Route("/goods/delete/{id<\d+>}", name="good_delete")
     */
    public function delete(int $id, ManagerRegistry $doctrine): Response
    {
        $good = $doctrine->getManager()->getRepository(Good::class)->find($id);

        if ($good == null) {
            $this->addFlash(
                "notice",
                "Товар с идентификатором " . $id . " не найден"
            );

            return $this->redirectToRoute('admin_goods');
        }

        $good_repository = new GoodRepository($doctrine);
        $good_repository->remove($good, true);

        $this->addFlash(
            "notice",
            "Товар успешно удален"
        );

        return $this->redirectToRoute('admin_goods', ['page' => 1]);
    }
}
