<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Good;
use App\Repository\CategoryRepository;
use App\Repository\GoodRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestDataController extends AbstractController
{
    /**
     * @Route("/test_data/fill", name="test_data_fill")
     */
    public function fill(ManagerRegistry $doctrine): Response
    {
        $categories = $doctrine->getRepository(Category::class)->findAll();
        $goods = $doctrine->getRepository(Good::class)->findAll();

        $category_repository = new CategoryRepository($doctrine);
        $good_repository = new GoodRepository($doctrine);

        $min_category_index = count($categories) + 1;
        $max_category_index = count($categories) + 5;

        for ($i = $min_category_index; $i <= $max_category_index; $i++) {
            $category = new Category();
            $category->setName('Категория ' . $i);
            $category_repository->add($category, true);
        }

        for ($i = count($goods) + 1; $i <= count($goods) + 15; $i++) {
            $good = new Good();
            $good->setName('Товар ' . $i);
            $good->setPrice(rand(100, 500));
            $good->setCategory($doctrine->getRepository(Category::class)->find(rand($min_category_index, $max_category_index)));
            $good_repository->add($good, true);
        }

        $this->addFlash(
            "notice",
            "Категории и товары успешно созданы"
        );

        return $this->redirectToRoute('goods');
    }
}
